package com.example.myapplication.network
import com.example.myapplication.model.PokeResponse
import com.example.myapplication.util.Constants
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @GET(Constants.POKE_API_URL+"{pokemon}")
    fun hitPokeData(@Path ("pokemon")pokemon:String): Call<PokeResponse>

}

