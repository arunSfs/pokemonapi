package com.example.myapplication.pokeData

import android.app.Activity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.model.PokeResponse
import com.example.myapplication.network.ApiInterface
import com.example.myapplication.util.ProgressDialog
import com.tapdatingapp.tapdating.network.ServiceClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokeViewViewModel(val context: Activity) : AndroidViewModel(context.application) {
    val pokeResponse = MutableLiveData<PokeResponse>()
    val existsError = MutableLiveData<String>()

    fun pokeData(name:String) {
        ProgressDialog.showLoading(context)
        ServiceClient.getClientHeader().create(ApiInterface::class.java).hitPokeData(name).enqueue(
            object : Callback<PokeResponse> {
                override fun onFailure(call: Call<PokeResponse>, t: Throwable) {
                    existsError!!.value = t.message
                    ProgressDialog.hideLoading()
                }
                override fun onResponse(call: Call<PokeResponse>, response: Response<PokeResponse>
                ) {
                    if (response.isSuccessful) {
                        pokeResponse.value = response.body()
                        ProgressDialog.hideLoading()
                    }else
                    {
                        ProgressDialog.hideLoading()

                    }
                }
            })
    }
}
