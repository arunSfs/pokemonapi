package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.MoveRowItemBinding
import com.example.myapplication.model.Move
import com.example.myapplication.model.PokeResponse
import com.example.myapplication.model.Stat
import com.example.myapplication.util.loadNetworkImage
import java.util.ArrayList


class StatAdapter(
    private var context: Context, var list: ArrayList<Stat>
) : RecyclerView.Adapter<StatAdapter.MyViewHolder>() {
    private lateinit var binding: MoveRowItemBinding

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val moveTV : TextView = itemView.findViewById(R.id.moveTV)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.move_row_item,
            parent,
            false
        )
        return MyViewHolder(binding.root)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       holder.moveTV.text=list[position].stat.name
    }
}