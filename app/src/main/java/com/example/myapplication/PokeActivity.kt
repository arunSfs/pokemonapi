package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.databinding.ActivityPokeBinding
import com.example.myapplication.model.Move
import com.example.myapplication.model.PokeResponse
import com.example.myapplication.model.Stat
import com.example.myapplication.pokeData.PokeViewViewModel
import com.example.myapplication.util.loadNetworkImage
import java.util.ArrayList

class PokeActivity : AppCompatActivity(), View.OnClickListener{
    private lateinit var binding: ActivityPokeBinding
    var viewmodel: PokeViewViewModel? = null
    lateinit var moveadapter: MoveAdapter
    lateinit var statadapter: StatAdapter
    val moveList = ArrayList<Move>()
    val statList = ArrayList<Stat>()

    var _count=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_poke)
        viewmodel = PokeViewViewModel(this)
        binding.viewModel = viewmodel

        initClick()
        pokeObserver()

    }

    private fun pokeObserver() {
        viewmodel!!.pokeResponse.observe(this) {
            moveList.clear()
            statList.clear()
            binding.pokeTV.text=it.name
            binding.pokeBackIV.loadNetworkImage(it.sprites.back_shiny)
            binding.pokefrontIV.loadNetworkImage(it.sprites.front_shiny)
            moveList.addAll(it.moves)
            statList.addAll(it.stats)
            moveadapter.notifyDataSetChanged()
            statadapter.notifyDataSetChanged()
        }
    }

    private fun initClick() {
        binding.fetchTV.setOnClickListener(this)
        viewmodel!!.pokeData("ditto")

        binding!!.moveRV.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        moveadapter = MoveAdapter(this,moveList)
        binding!!.moveRV.adapter = moveadapter

        binding!!.statRV.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        statadapter = StatAdapter(this,statList)
        binding!!.statRV.adapter = statadapter
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.fetchTV -> {
                _count=_count+1
                viewmodel!!.pokeData(_count.toString());
            }
        }
    }
}