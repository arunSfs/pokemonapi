package com.example.myapplication.util

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.example.myapplication.R

public class ProgressDialog {

    companion object {
        private var mProgressDialog: ProgressDialog? = null

    fun showLoading(context:Context,title:String="") {
        hideLoading()
        mProgressDialog = initProgressDialog(context,title)
    }

    fun hideLoading() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.cancel()
        }

    }
    fun initProgressDialog(context: Context, title: String): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(/*if (title.isEmpty())*/ R.layout.spinner_loader_dialog /*else R.layout.loader_dialog*/)
        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        return progressDialog
    }

    }

}