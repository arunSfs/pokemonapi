package com.example.myapplication.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.R



fun ImageView.loadNetworkImage(imgPath: String?) {
    if (imgPath!!.equals("") || imgPath.isEmpty()) {
        Glide.with(this)
            .setDefaultRequestOptions(RequestOptions().placeholder(R.mipmap.ic_launcher))
            .load(imgPath)
            .into(this)

    } else {
        Glide.with(this).setDefaultRequestOptions(RequestOptions().placeholder(R.mipmap.ic_launcher))
            .load(imgPath)
            .into(this)
    }
}


