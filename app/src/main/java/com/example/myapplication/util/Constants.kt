package com.example.myapplication.util

class Constants {
    companion object {
        //App Base URl
        const val BASE_URL="https://pokeapi.co/"
        //End URl
        const val POKE_API_URL="api/v2/pokemon/"
    }
}